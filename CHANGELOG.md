## [0.1.5](https://gitlab.com/msvechla/go-vulncheck-gitlab/compare/0.1.4...0.1.5) (2022-10-30)


### Bug Fixes

* **docker:** cleanup Dockerfile ([315090c](https://gitlab.com/msvechla/go-vulncheck-gitlab/commit/315090c6c1eac11569f9f80badef83ee17934383))
* **entrypoint:** shellcheck findings ([3097f8e](https://gitlab.com/msvechla/go-vulncheck-gitlab/commit/3097f8e9194d678b8817bb3f2a44269f2188747a))
* **go:** update dependencies ([fbebe43](https://gitlab.com/msvechla/go-vulncheck-gitlab/commit/fbebe43ac4d0f94d356e6c4da1ef73ff0a49cdef))

## [0.1.4](https://gitlab.com/msvechla/go-vulncheck-gitlab/compare/0.1.3...0.1.4) (2022-10-12)


### Bug Fixes

* **analyzer:** add missing alias -f for file flag ([89ac995](https://gitlab.com/msvechla/go-vulncheck-gitlab/commit/89ac99587bfaa78da8bdd39c76579729141b71f1))

## [0.1.3](https://gitlab.com/msvechla/go-vulncheck-gitlab/compare/0.1.2...0.1.3) (2022-10-12)


### Bug Fixes

* **ci:** fix entrypoint in gorelease build ([4f5239d](https://gitlab.com/msvechla/go-vulncheck-gitlab/commit/4f5239d3146a0721fa81c62af1f440aa42ae9c0b))
* **license:** add license ([1343445](https://gitlab.com/msvechla/go-vulncheck-gitlab/commit/1343445faeb92dfbcd7be03dbd081b93f6baa39c))

## [0.1.2](https://gitlab.com/msvechla/go-vulncheck-gitlab/compare/0.1.1...0.1.2) (2022-10-12)


### Bug Fixes

* **ci:** fix release ([3d22cf4](https://gitlab.com/msvechla/go-vulncheck-gitlab/commit/3d22cf4f8ec4a883ebc3e6ac882568ff8a2be2d4))

## [0.1.2](https://gitlab.com/msvechla/go-vulncheck-gitlab/compare/0.1.1...0.1.2) (2022-10-12)


### Bug Fixes

* **ci:** fix release ([3d22cf4](https://gitlab.com/msvechla/go-vulncheck-gitlab/commit/3d22cf4f8ec4a883ebc3e6ac882568ff8a2be2d4))

## [0.1.1](https://gitlab.com/msvechla/go-vulncheck-gitlab/compare/0.1.0...0.1.1) (2022-10-11)


### Bug Fixes

* **anayzer:** automatic version update ([829aead](https://gitlab.com/msvechla/go-vulncheck-gitlab/commit/829aeadff4253c7c4d2d17dc0ac22a4fc2f656e6))

## [0.1.0](https://gitlab.com/msvechla/go-vulncheck-gitlab/compare/0.0.1...0.1.0) (2022-10-11)


### Features

* **ci:** add goreleaser ([ff889f3](https://gitlab.com/msvechla/go-vulncheck-gitlab/commit/ff889f385ab658ad91f0ee4b66d9ef0ccaa9e546))
* **ci:** add semantic-release ([18102ca](https://gitlab.com/msvechla/go-vulncheck-gitlab/commit/18102ca9270649860b271e4cf96ede3e33ee59df))


### Bug Fixes

* **analyzer:** add versioning to binary / cli ([95c078c](https://gitlab.com/msvechla/go-vulncheck-gitlab/commit/95c078c6101afa854ddad6025473f591c04b0e1f))
