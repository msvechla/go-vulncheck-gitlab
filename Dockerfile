## Builder Image
FROM golang:1 as builder
RUN apt update && apt install -y --no-install-recommends --upgrade openssl

# Install Upstream govulncheck with testmode
WORKDIR /
RUN git clone https://github.com/golang/vuln.git
RUN cd vuln/cmd/govulncheck && CGO_ENABLED=0 GO111MODULE=on GOOS=linux go build -a -installsuffix cgo -o govulncheck -tags testmode .

## Application Image
FROM golang:1.19

RUN mkdir -p /home/app && \
    addgroup app && \
    adduser --ingroup app app

RUN apt update && apt install -y --no-install-recommends --upgrade jq \
  && rm -rf /var/lib/apt/lists/*

WORKDIR /home/app
COPY --from=builder /vuln/cmd/govulncheck/govulncheck /usr/local/bin/
COPY entrypoint.sh /usr/local/bin/entrypoint.sh
COPY go-vulncheck-gitlab /usr/local/bin/

RUN chown -R app:app /home/app && chown -R app:app /usr/local/bin
USER app
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
