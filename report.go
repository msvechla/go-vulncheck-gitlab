package main

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"time"

	"gitlab.com/msvechla/go-vulncheck-gitlab/schema"
	"golang.org/x/vuln/osv"
)

const (
	reportTimeFormat = "2006-01-02T15:04:05"
)

// generateReport generates a gitlab dependency scanning report
func generateReport(s *Summary, startTime string) (*schema.ReportFormatForGitLabDependencyScanning, error) {
	dependencyfiles := generateDependencyFiles(s)
	vulns, err := generateVulnerabilities(s)
	if err != nil {
		return nil, fmt.Errorf("generating vulnerabilities: %w", err)
	}

	report := schema.ReportFormatForGitLabDependencyScanning{
		Version:         "15.0.2",
		DependencyFiles: []*schema.DependencyFilesItems{&dependencyfiles},
		Vulnerabilities: vulns,
		Schema:          "https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/master/dist/dependency-scanning-report-format.json",
	}
	report.Scan = generateScanInfo(startTime, time.Now().Format(reportTimeFormat))
	return &report, nil
}

// generateScanInfo generates a schema.Scan
func generateScanInfo(startTime string, endTime string) *schema.Scan {
	return &schema.Scan{
		Analyzer: &schema.Analyzer{
			Id:      "govulncheck",
			Name:    "govulncheck",
			Url:     "https://github.com/golang/vuln",
			Version: "b5628b3a3e55af77e5d5e2589064e35b438086a9",
			Vendor: &schema.Vendor{
				Name: "The Go Authors",
			},
		},
		Scanner: &schema.Scanner{
			Id:      "go-vulncheck-gitlab",
			Name:    "go-vulncheck-gitlab",
			Url:     "https://gitlab.com/msvechla/go-vulncheck-gitlab",
			Version: "0.0.1",
			Vendor: &schema.Vendor{
				Name: "msvechla",
			},
		},
		Status:    "success",
		StartTime: startTime,
		EndTime:   endTime,
		Type:      "dependency_scanning",
	}
}

// generateVulnerabilities generates schema.VulnerabilitiesItems based on a govulncheck Summary
func generateVulnerabilities(s *Summary) ([]*schema.VulnerabilitiesItems, error) {
	vulns := []*schema.VulnerabilitiesItems{}
	for _, a := range s.Affecting {

		callStacks, err := generateCallStacks(a)
		if err != nil {
			return nil, fmt.Errorf("generating callstacks: %w", err)
		}

		v := schema.VulnerabilitiesItems{
			Description: generateDescription(a.OSV.Details, a),
			Id:          a.OSV.ID,
			Name:        a.OSV.ID,
			Identifiers: generateIdentifiers(a.OSV),
			Links:       generateLinks(a.OSV.References),
			Location: &schema.Location{
				File: "go.mod",
				Dependency: &schema.Dependency{
					Package: &schema.Package{
						Name: a.ModPath,
					},
					Version: a.FoundIn,
				},
			},
			Solution: generateSolution(a),
			Tracking: nil,
			Severity: "Unknown",
			Details: &schema.VulnerabilityDetails{
				CallStacks: &schema.VulnerabilityLocation{
					Name:  "callstacks",
					Type:  "list",
					Items: callStacks,
				},
			},
		}
		vulns = append(vulns, &v)
	}
	return vulns, nil
}

// generateSolution generates info on how to resolve a vulnerabilities based on a Vuln
func generateSolution(v Vuln) string {
	if v.FixedIn != "" {
		return fmt.Sprintf("upgrade to version %s", v.FixedIn)
	}
	return "no solution available yet"
}

// generateCallStacks generates call stacks in the form of schema.FileLocations from a Vuln
func generateCallStacks(v Vuln) (*[]*schema.FileLocation, error) {
	traces := generateUniqueTraces(v)
	callStacks := []*schema.FileLocation{}
	for _, t := range traces {
		cs, err := traceToFileLocation(t)
		if err != nil {
			return nil, err
		}
		callStacks = append(callStacks, cs)
	}
	return &callStacks, nil
}

// traceToFileLocation generates a schema.FileLocation from a trace
func traceToFileLocation(trace string) (*schema.FileLocation, error) {
	r := regexp.MustCompile(`(.*\.go):(\d+):(\d+):.*`)
	matches := r.FindStringSubmatch(trace)

	if len(matches) != 4 {
		return nil, fmt.Errorf("parsing trace location: %s", trace)
	}

	file := matches[1]
	start := matches[2]
	startLine, err := strconv.Atoi(start)
	if err != nil {
		return nil, fmt.Errorf("parsing start line: %w", err)
	}

	return &schema.FileLocation{
		FileName:  file,
		LineStart: &startLine,
		LineEnd:   &startLine,
		Type:      "file-location",
	}, nil
}

// generateDescription generates the vulnerability descriptions
// based on general details and the vulns callstacks
func generateDescription(details string, v Vuln) string {
	sb := strings.Builder{}

	sb.WriteString(details)
	sb.WriteString("\n")
	sb.WriteString("affected call sites in your code:\n")

	traces := generateUniqueTraces(v)
	for _, t := range traces {
		sb.WriteString(fmt.Sprintf("- %s\n", t))
	}
	return sb.String()
}

// generateUniqueTraces returns a list of unique traces from a Vuln
func generateUniqueTraces(v Vuln) []string {
	traces := map[string]bool{}
	for _, t := range v.Trace {
		traces[t.Desc] = true
	}

	uniqueTraces := make([]string, len(traces))
	i := 0
	for t := range traces {
		uniqueTraces[i] = t
		i++
	}
	return uniqueTraces
}

// generateIdentifiers generates schema.IdentifiersItems from an osv.Entry
func generateIdentifiers(o *osv.Entry) []*schema.IdentifiersItems {
	ids := []*schema.IdentifiersItems{}

	url := ""
	if len(o.Affected) > 0 {
		url = o.Affected[0].DatabaseSpecific.URL
	}

	ids = append(ids, &schema.IdentifiersItems{
		Name:  o.ID,
		Type:  "go",
		Value: o.ID,
		Url:   &url,
	})

	for _, a := range o.Aliases {
		ids = append(ids, &schema.IdentifiersItems{
			Name:  a,
			Type:  parseIdentifierType(a),
			Value: a,
		})
	}

	return ids
}

// generateLinks generates schema.LinksItems from osv.References
func generateLinks(references []osv.Reference) []*schema.LinksItems {
	li := []*schema.LinksItems{}

	for _, r := range references {
		li = append(li, &schema.LinksItems{
			Url: r.URL,
		})
	}
	return li
}

// parseIdentifierType parses the identifier short key from a full identifiert
func parseIdentifierType(identifier string) string {
	return strings.ToLower(strings.Split(identifier, "-")[0])
}

// generateDependencyFiles generates a list of dependencies from a Summary
func generateDependencyFiles(s *Summary) schema.DependencyFilesItems {
	d := schema.DependencyFilesItems{
		PackageManager: "go",
		Path:           "go.mod",
	}

	modVersions := map[string]string{}
	for _, a := range s.Affecting {
		modVersions[a.ModPath] = a.FoundIn
	}

	deps := make([]*schema.DependenciesItems, len(modVersions))

	i := 0
	for mod, version := range modVersions {
		deps[i] = &schema.DependenciesItems{
			Package: &schema.Package{
				Name: mod,
			},
			Version: version,
		}
		i++
	}
	d.Dependencies = deps
	return d
}
