# go-vulncheck-gitlab

[![pipeline status](https://gitlab.com/msvechla/go-vulncheck-gitlab/badges/main/pipeline.svg)](https://gitlab.com/msvechla/go-vulncheck-gitlab/commits/main) [![coverage report](https://gitlab.com/msvechla/go-vulncheck-gitlab/badges/main/coverage.svg)](https://gitlab.com/msvechla/go-vulncheck-gitlab/commits/main) [![go report](https://goreportcard.com/badge/gitlab.com/msvechla/go-vulncheck-gitlab)](https://goreportcard.com/report/gitlab.com/msvechla/go-vulncheck-gitlab) [![Docker Pulls](https://badgen.net/docker/pulls/msvechla/go-vulncheck-gitlab)](https://hub.docker.com/r/msvechla/go-vulncheck-gitlab)

A custom Gitlab dependency scanner analyzer for [govulncheck](https://pkg.go.dev/golang.org/x/vuln/cmd/govulncheck).

> Govulncheck reports known vulnerabilities that affect Go code. It uses static analysis of source code or a binary's symbol table to narrow down reports to only those that could affect the application.

The dependency scanner [govulncheck](https://pkg.go.dev/golang.org/x/vuln/cmd/govulncheck) is a great tool for minimizing false-positives in dependency scanning results. The `go-vulncheck-gitlab` custom analyzer extends the Gitlab dependency scanning by running `govulncheck` and finally parsing the output and formatting it according to [Gitlabs dependency scanning report format](https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/main/dist/dependency-scanning-report-format.json).

## Usage

The `govulncheck_dependency_scanning` job can be included by simply extending your existing `.gitlab-ci.yml` with the following snippet:
```yaml
govulncheck_dependency_scanning:
  stage: test
  image: 
    name: msvechla/go-vulncheck-gitlab:0.1.5
    entrypoint: [""]
  allow_failure: true
  artifacts:
    reports:
      dependency_scanning: gl-dependency-scanning-report.json
  script:
    - /usr/local/bin/entrypoint.sh
  rules:
    - if: $DEPENDENCY_SCANNING_DISABLED
      when: never
    - if: $DS_EXCLUDED_ANALYZERS =~ /go-vulncheck-gitlab([^-]|$)/
      when: never
    - if: $CI_COMMIT_BRANCH &&
          $GITLAB_FEATURES =~ /\bdependency_scanning\b/ &&
          $CI_GITLAB_FIPS_MODE == "true"
      exists:
        - '**/*.go'
    - if: $CI_COMMIT_BRANCH &&
          $GITLAB_FEATURES =~ /\bdependency_scanning\b/
      exists:
        - '**/*.go'
```

## Preview

The scan summary will be highlighted in Gitlab as usual:

![scan overview](./docs/scan_overview.png)

Checking out the scan details will also reveal the call-stacks where vulnerable dependencies are called in your code:

![scan details](./docs/scan_details.png)

## Caveats

`govulncheck` is still under heavy development and has no useful public API at the moment. Therefore we currently compile it in test mode, so we can get usefule `JSON` output.
As we have to do some plumbing between the `govulncheck` binary and running our analyzer, the container image is quite large. This will be fixed in the future, when we can call the relevant APIs directly in go.

## References

- [govulncheck](https://pkg.go.dev/golang.org/x/vuln/cmd/govulncheck)
- [Go Vulnerability Management](https://go.dev/security/vuln/)
- [Gitlab Security Scanner Integration](https://docs.gitlab.com/ee/development/integrations/secure.html)

