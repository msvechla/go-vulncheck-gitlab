build-analyzer:
	sudo docker build -t analyzer .

build-validator:
	sudo docker build -t validator -f Dockerfile.validate .

test: build-analyzer build-validator
	sudo docker run -it -v ${PWD}/testdata/manystacks/:/home/app analyzer
	sudo docker run -it -v ${PWD}/testdata/manystacks/gl-dependency-scanning-report.json:/ci/gl-dependency-scanning-report.json validator
