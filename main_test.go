package main

import (
	"reflect"
	"testing"

	"github.com/xeipuuv/gojsonschema"
	"gitlab.com/msvechla/go-vulncheck-gitlab/schema"
)

func Test_traceToFileLocation(t *testing.T) {
	type args struct {
		trace string
	}
	tests := []struct {
		name    string
		args    args
		want    *schema.FileLocation
		wantErr bool
	}{
		{
			name: "example",
			args: args{
				trace: "otherpkg/otherpkg.go:6:19: example.com/manystacks/otherpkg.GetPeers calls github.com/shiyanhui/dht.DHT.GetPeers",
			},
			want: &schema.FileLocation{
				FileName:  "otherpkg/otherpkg.go",
				LineStart: intP(6),
				LineEnd:   intP(6),
				Type:      "file-location",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := traceToFileLocation(tt.args.trace)
			if (err != nil) != tt.wantErr {
				t.Errorf("traceToFileLocation() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("traceToFileLocation() = %v, want %v", got, tt.want)
			}
		})
	}
}

func intP(i int) *int {
	return &i
}

func Test_generateReportBytes(t *testing.T) {
	schemaLoader := gojsonschema.NewReferenceLoader("file://./schema/dependency-scanning-report-format.json")

	type args struct {
		summaryFilePath string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{name: "maystacks", args: args{summaryFilePath: "./testdata/manystacks-summary.json"}, wantErr: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := generateReportBytes(tt.args.summaryFilePath)
			if (err != nil) != tt.wantErr {
				t.Errorf("generateReportBytes() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			documentLoader := gojsonschema.NewStringLoader(string(got))
			result, err := gojsonschema.Validate(schemaLoader, documentLoader)
			if (err != nil) != tt.wantErr {
				t.Errorf("generateReportBytes() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if (!result.Valid()) != tt.wantErr {
				t.Errorf("generateReportBytes() validation error = %v, wantErr %v", result.Errors(), tt.wantErr)
				return
			}
		})
	}
}
